#!/bin/chez/

(load "./myenv.scm") ;comfy shorthands

(define (cut-at l needle)
 (if (or ( equal? needle (car l))
	 ( null? l))		 
      '()
      (cons (car l) (cut-at (cdr l) needle))))

(define (cut-after l needle)
 (if  (equal? needle (car l)) (cdr l)
	(if (null? l) '() (cut-after (cdr l) needle))))

(define (last-satisfies l testprocedure)
 (let [(λfind '())
       (λlast-satisfies [λ (λl testprocedure) [
	(if (null? λl)  λfind ;return once done
			(if (testprocedure (car λl)) (set! λfind (car λl))))]])]
	[λlast-satisfies l testprocedure]))

(define (last-function l cf)
 ( if (null? l) cf
 (last-function (cdr l)
  (if (procedure? (car l)) (car l) cf))))


(define (get-firsts l n)
 (if (or ( zero? n) (null? l))
	'()
	(cons (car l) (get-firsts (cdr l) (- n 1)))))
(define (cut-lasts l n)
(get-firsts l (- (length l) n)))

(define (get-after l n)
 (if (or (zero? n ) (null? l))
     l
     (get-after (cdr l) (- n 1))))
(define (get-lasts l n)
 (get-after l (- (length l) n)))
(define (get-nargs l n)
 (get-lasts (cut-lasts l 2) n ))

(define (thereisno l needle)
 (cond  ((null? l) #t)
   	((equal? (car l) needle) #f)
	(else (thereisno (cdr l) needle))))

(define lsd-needle '!)


(define (lsd l)
  (if (thereisno l lsd-needle) l
   (let* [(λcurrent (cut-at l lsd-needle))
	  (λafter (cut-after l lsd-needle))
	  (λop (get-lasts λcurrent 2))
	  (λproc (car λop))
	  (λargs (get-nargs λcurrent (cadr λop)))
	  (λbefore (cut-lasts λcurrent (+ (cadr λop) 2)))]
   ( lsd  (append λbefore 
		  (if (member λproc stackprocs) ;stack handling metaprocedures go here
		   (λproc (car (get-lasts  λargs 1)) (get-firsts λargs (- (length λargs) 1)))
		   (list (apply λproc λargs)))
		  λafter)))))
  ;(cut-at l lsd-needle)

;;lsd recommended builtins
(define (nop . r) r)
(define (rot n l)
  (define ( λrotr λn λl)
   (if (zero? λn) λl
    (λrotr (- λn 1) (append (cdr λl) (list (car λl))))))
 (λrotr (mod (- n) (length l)) l) ; = (rotl n l)
  )

(define ( dup n l) ; n (l) -> (l l l..)length=n
  (define (λdupn λn λl) 
 (if (zero? λn) '() (cons λl (λdupn (- λn 1) λl))))
	[apply append (λdupn n l)])

(define ( dupn n l) ; n (l) -> (l l l l.. n)length=n+1
 (append  (dup n l) (list n)) 
  )

(define stackprocs (list rot dup dupn)) ; procedure list, not symbol list
;;
;Glorified repl features:

(define ! '!) ;allow ! to be the caller
(define (lsd-repl)
  (display "|lsd|")
  (let ([exprl (read)])(if (equal? exprl ':help) ( display "
this is the lsd repl.
lsd syntax works like this:
(1 2 3 + 3 !) ; calculate 1+2+3 =>.
1 2 3 are arguments to + .
! lets you call the last function you mentioned, here + .
the 3 between + and ! is the argument count; it's a meta-argument, and
currently the only kind of meta-argument in lsd.scm .
comments aren't supported by lsd.scm but the scheme repl might handle it
for you. it's also common practice to use ; to denote comments on the right of
the ; .
lsd programs always results in a list. thus, dead code cannot exist as it's
either:
-used to calculate a result
-is a result.
variables and functions cannot be created in lsd (the latter might change).
thus, lsd can be used as a restricted shell language by cautiously curating
the envtable of the program it runs in.
in comments, => means 'and return it/that as part of the list lsd gives back'.
here's a few extra examples for your immersion:
(read 0 !) ; read input =>.
(4 2 dup 2 ! * 2 !) ; dupe 4 and multiply it by itself =>. (aka 'square of 4')
; note that dup's behaves like so:
(\"he\" dup 2 !) ; \"he\" \"he\" =>. 
(\"he\" dup 1 !) ; \"he\" =>.
(\"he\" dup 0 !) ; nothing =>. (thus we get back an empty list) 
(4 3 dup 2 ! * 3 !) ; cube of 4. 
; for long code it's better to split it in multiple lines:
(   read 0 !    ; lets name this x. our buffer: x 
    read 0 !    ; lets name this y. x,*,y
    dupm 2 !    ; x in succession y times, y
* 1 rot  3 !    ; x in succession y times, *, y ; also known as (x x x... x * y) 
           !)   ; x*x*...*x == x^y == (x x x... x * y !)
good luck with lsd!
")
  (display (lsd (map eval exprl)))))
  (display "\n")
  (lsd-repl))
