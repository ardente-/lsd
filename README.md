# lsd

lsd | bsl : backwards-stack-lisp (aka. reverse polish scheme)
programming language* that's a middle ground between scheme/lisp and forth.

currently implemented in Scheme

![pilllogo](/lsd.png "Logo")
## *programming language:

lsd is more of two (and a half) programming "paradigms" (neat tricks) combined together than a language:
1. Stack-based programming
2. Functional programming

(And optionally, metaprogramming.)

The concept is very simple: lsd aims to extend reverse polish notation into a programming paradigm. 

Any language capable of expressing first-class functions and stack(-like) data structures should be usable to implement its own flavor of lsd.

## Running 
### Setting up

lsd (the concept) has the following dependencies:
- A thing it can run on
- Someone willing to implement it on said thing if it isn't already

More usefully, the official implementation, lsd.scm (lsd, scheme flavour) has the following dependencies:
- A scheme interpreter/compiler capable of running it

The supported scheme interpreter is Chez Scheme (version 9.5.6, currently).

Clone and cd into this repository locally with

`git clone https://gitlab.com/ardente-/lsd && cd lsd`

(May vary for non-Unix-like systems)

Feel free to hack the source code with your favorite editor, such as `ed`.
### Getting to the REPL/interactive prompt
After having cloned and cd'd into the repo, run the following: 

`scheme repl.scm`  (or `chez repl.scm` )

### Learning

TODO ; check out docs/
