# lsd | bsl : backwards-stacked-lisp
## Introduction

Welcome to the backwards-stacked-lisp documentation. A quick primer is located below.

## Primer (scheme)

A lsd program is a data structure, its output is also a data structure.

### Theory class
Data structures are stuff that structure data.

You could put it in a tree (it looks like a biological tree, with data hanging off where would be relatives that don't have an offspring in that case) or lists (a shopping list is an unordered list; it's not specifically because eggs are at the top that you will buy eggs first.)

Our data structure of choice is the list. Let's call stuff in parentheses lists.

`('this 'is 'a 'list)`


This list has the following elements:
1. 'this
2. 'is
3. 'a 
4. 'list

`( '('this 'is 'a 'list) 'inside 'a 'list)`

This list has the following elements:
1. 'A list, that looks exactly like the previous list we just saw.
2. 'inside
3. 'a
4. 'list

The little ' (quote) is a thing you needn't care about for now, it'll be explained later.

Data can be various things. Weird people (read: lisp/scheme programmers) call data that aren't data structures (in our case, lists) "atoms".

While this all may sound scary, atoms are just "stuff that aren't lists".

For example:
- "a" - atom (we call it a string; it's just text)
- 2 - atom (we call it a number (or integer))
- -2.95758 - atom (we call it a number (or float))
- ("definitely not a list") - list (of one element)
- 'definitely-not-an-atom - atom (we call that a symbol ; more on that later)
- 1 2 3 4 - four atoms
- #&lt;procedure +> - atom (we call it a procedure, or function (it lets us do stuff))

We can now talk about functions (aka procedures, for the sake of simplicity).

Functions let us do stuff. Addition could be a function.

Functions are things , like lists, trees, data structures, atoms, numbers, ... ; they're not spared from being manipulated and thrown around.

For instance, here's a list of all basic mathematical operations:

`(#<procedure +> #<procedure -> #<procedure *> #<procedure />)`
This is where I get to explain symbols. Bear with me.

The + sign character is not the same as addition; it's not the actual character that grabs two numbers and throws the result of adding them at you, it's your brain seeing the + symbol that does because your brain goes "Oh, that's a plus symbol, I'll just do addition".

lsd (and lisp and scheme) do something similar. If I gave you or scheme the following:

 2 + 3 
 
without context, and asked you what we were doing here, both of you would go "Hey, that's addition, we're adding stuff". I'd have to explicitly tell you and scheme "no, that's the plus symbol, not the act of adding addition" for you two to understand I want to refer to the sign rather than the act. ' (quote) does exactly that; '+ refers to the plus sign, and + to the actual operation.

### In lsd

lsd goes a little deeper with symbolism; while that mostly holds true, lsd will not go "we're adding stuff" and add them like scheme, but instead go "this is addition, with 2 to the left and 3 to the right." . 

You have to use what we call the "caller" (along with what we call the argument count (or meta-argument in the source code)) to make it actually do a thing. Here is an example (remember, lsd programs are lists!):
`
`( 2 3 #<procedure +> )`

You can input it as `( 2 3 + )` , the program does the job for you of translating your + into a procedure.

To lsd, this is a list of:
- the number, 2
- the number, 3
- the procedure/function, #&lt;procedure +> , also known as addition

It returns itself because nothing has changed; so the result of ( 2 3 + ) is ( 2 3 + ) , which also makes it a quine (programs that return their own source code). 

To force lsd to do anything, you must yell the number of arguments a function takes at it with the "caller" . For instance, we're adding 2 and 3 together; thus, the number of arguments is 2. The default and recommended caller is ! .

`( 2 3 + 2 !)`
Returns (5).

By leveraging how nice scheme is about adding multiple numbers together, we do that very easily in lsd:
`( 1 9 -4 0.2 55005.5 + 5 !)`
Returns (55011.7).

You may have noticed lsd returns a list if it has one result. That's intentional.

Here is how it looks like if it has more than one result:

`( 1 9 -4 0.2 55005.5 + 4 !)`

Returns (1 55010.7).

TODO

### Advanced
Variable declaration does not exists within lsd. This is intended; lsd is meant to be quasi-purely functional and variables are side effects.

There are three fixes to this:
1. Manipulate the program's stack with skill.
2. Meta-program lsd's environment with the host language (here, scheme) at runtime (here, using eval) .
3. Implement it yourself, somehow. It's FOSS.

