dofile("lsd.lua")
print("lsd.lua")
--standard functions:
_G["+"] = function(s,b,...)
	if ... then return s+_G["+"](...)
	else return s end end
_G["-"] = function(s,...)
	if ... then return s-_G["+"](...) 
	else return -s end end--lisp behavior
_G["*"] = function(s,...)
	if ... then return s*_G["*"](...)
	else return s*b*_G["+"](...) end end
_G["/"] = function(s,...)
	if ... then
		return s/(_G["*"](...)) --mult faster than div so
	else return 1/s end end--lisp behavior

function lsd_repl()
	io.write("|lsd|")
	local input = io.read()
	if input ~= ":q" then
	print("> (",table.concat(eval(parse(tknize(input)))),")")
	lsd_repl() end
	print("|lsd-goodbye|") end
print("loaded")
lsd_repl()
