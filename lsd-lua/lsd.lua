dofile("main.lua")
local env = _G --configure this
function child(t,str,delim)
	for target in str:gmatch("[^"..delim.."]+") do
		if type(t)=="table" then --intentionally returns nil sometimes.
		t = t[target] end end return t end
function nargs(f) --<f=f if type(f)=="function"
	if debug.getinfo(f).nparams==0 and debug.getinfo(f).isvararg then return false--false is useful for call.
	 else return debug.getinfo(f).nparams end end
function tknize(str) --<str=io.read() or ? if type(?)=="string"
	return str:gmatch("%S+") end
function escapes(spacelessstr) --<str if str:find(" ")==nil and type(str)=="string"
	return spacelessstr	:gsub("\\\\"," \\\\ "):gsub("\\t","\t")
				:gsub("\\n","\n"):gsub("\\(%x+)",function(s) 
				 return utf8.char(tonumber("0x"..s)) end) --replace \ff3 by unicode codepoint ff3 .
				:gsub(" \\\\ ", "\\") end
function parse(iter) --<iter=tknize(str)
	local ast = {}
	for tkn in iter do
	table.insert(ast,(function()
			return cond(	tonumber(tkn)~=nil,function()return{value=tonumber(tkn),type="number"}end
					,tkn=="!",function() return {value="!",type="!"} end--callneedle
					,tkn:sub(1,1)=="\"",function() local str=tkn
					 while str:sub(-1)~="\"" or str:sub(-2)=="\\\""do 
					  str=str.." "..escapes(iter())end return {value=str:sub(2,-2),type="string"} end)
			or function()return {value=tkn,type="symbol"} end end)()(tkn)) end 
	return ast end
function eval(ast)
	local stack={}
	--for head,object in pairs(ast) do
	local head = 1
	while head <= #ast do --more flexible? iunno
		local object = ast[head]
		--print(object.value)
		--if type(object.type) == "table" then print(table.unpack(object.type)) end
		cond(	object.type=="number" or object.type=="string",function()ast[head]=object.value end
			--,object.type=="string",function()ast[head]=object.value end
			,object.type=="symbol" and object.value:sub(1,1)~="'",function()ast[head]=child(env,object.value,".")end
			,object.type=="symbol" and object.value:sub(1,1)=="'",function()ast[head].value=object.value:sub(2)end
			,object.type=="!",function()--caller detected, running function
			 local f,fargs
			 if type(ast[head-2])~="function" then --argcount unspecified
			 table.insert(ast,head,nargs(ast[head-1]) or head-2) --add argcount
			 head=head+1
			 end
			f,fargs=ast[head-2],ast[head-1] 
			--print(f,fargs)
			print(fargs)
			if type(fargs)=="table" then print(unroll(fargs)) end
			assert(type(f)=="function")
			--print("!!!",select(-fargs or 1,tceles(head-3,table.unpack(ast))))
			local results = {f(select(-fargs,tceles(head-3,table.unpack(ast))))}
			--print(table.unpack(results))
			local argshead = head-2-fargs --the first arg's position.
			for i = 1,fargs+3 do --[[print("<",i,ast[argshead])]] table.remove(ast,argshead) end -- now the next arg is at [argshead]. also, 3 is for off-by-one error correction and handling the function, meta-arg and 
			for k,val in ipairs(results) do --[[print(">",argshead,k,val)]] table.insert(ast,argshead+k-1,val) end -- gotta increment each time i add a new one; also, off-by-one error correction
			head=head+#results-3-fargs --Backtrack because disappearing function spot. Requires while loop, I believe.
			 end,true,function()end)() head=head+1 end return ast end
--DEBUGGING:
function unroll(t,depth)
	local depth=depth or 0
	for k,v in pairs(t) do
		print(("|\t"):rep((depth))..tostring(k),tostring(v)..":"..type(v):sub(1,3))
	if type(v)=="table" then unroll(v,depth+1) end end end
function list(t) local s = "(" for k,v in pairs(t) do s=s.." "..v end print(s,")") end



