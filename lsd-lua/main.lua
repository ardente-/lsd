#!/bin/luajit
--[[Starbase is a runtime extension for lua which takes care of repetitive
--boilerplate. This lets you write in a functional-like paradigm and make
--your programs get straight to the point instead of handling nils and such.]]

function iftf(bool,fn,...)
	if bool == true then
		return fn(...)
	end
end
function ift(bool,...)
	if bool == true then
		return ...
	end
end
function tceles(n,x,...)
	if n <= 0 then return end
	return x,tceles(n-1,...)
end
function ifte(bool,...)
	if bool == true then
		return tceles(select("#",...)/2,...)--WIA floating point
	else	return select(-math.floor(select("#",...)/2),...) end --WIA floor
end	
function may(fn,...)
	if ... ~=nil then return fn(...) end --This one was vicious, because if the first thing in ... is a bool, it would have evaluated to false and thus not returned. So the ~=nil is mandatory.
end
function may_(fn,...)
	if ... then return fn(...) else return fn end
function pip(cfn,...)
		 if ... then
			local smf = table.pack(...)
			return function(...) 
				return f(smacro(unpack(smf))(...))
			end
		else return function(...)
				return f(...)
			end
		end 
	end
end
function condf(bool,f,tableargs,...)
	if bool then return f(table.unpack(tableargs)),may_(condf,...) 
	 else return may_(condf,...) end end
	
function cond(bool,v,...)
           if bool then return v,may(cond,...) -- something not false is true. a la lisp.
            else return may(cond,...) end end

apply = ifte(table.foreach~=nil,table.foreach,function(tb,fn) for k,v in pairs(tb) do tb[k]=fn(k,v) end return tb end) --lisp habits die hard

function string.gsplit(str,pat) -- courtesy of lua-users.org
   local t = {}
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t, cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end
