(display "Copyright 2022 Ardente (c)
   __      ____   __
  / /     / __/__/ / GNU Affero v3
 / /__ __/ /  /   / ver 0.2.2 | :help
/____//___/  /___/ backwards-stack-lisp
.." )
(load "./lsd.scm")
(display "!\n")
;   __      ____   __
;  / /     / __/__/ /
; / /__ __/ /  /   /
;/____//___/  /___/
(lsd-repl)
